var express = require('express');
var request = require('request');
var router = express.Router();
var async = require('async');

var serverAddress = require('../utilities/settings').serverAddress;

var utilities = require('../utilities/index');

router.get('/', function(req, res) {
    var title = "";

    if (utilities.badUserNames(req.query.user)) {
        renderEmpty(res);
    } else {
        if (req.query.user && req.query.user != undefined) {
            renderWithData(res, req);
        } else {
            renderEmpty(res);
        }
    }
});

function renderEmpty(res) {
    res.render('index', {
        title: '#feedolist | instagram lists',
        users: 'empty',
        metadata: '#feedolist instagram photos',
        htmlToTable: ''
    });
}

function renderWithData(res, req) {
    request.get(
        uri = serverAddress + "web/posts?" + utilities.formatRequest(req.query.user),
        function(error, response, body) {

            if (error) {
                console.log('Error getting response from server: ' + error);
            } else {
                console.log('Response body: ' + body);
            }

            if (response) {
                var postsList = [];
                var allPosts = JSON.parse(response.body).postsList;
                for (var i = 0; i < allPosts.length; ++i) {
                    allPosts[i].posts.forEach(post => {
                        postsList.push(post);
                    });
                }
                postsList.sort(function(a, b) {
                    // Turn your strings into dates, and then subtract them
                    // to get a value that is either negative, positive, or zero.
                    return new Date(b.date) - new Date(a.date);
                });

                var usersInPosts = JSON.parse(response.body).users;

                /*
                var metadata = '';
                usersInPosts.forEach(user => {
                    if (user) metadata += ' (@' + user.username + ')'
                });
                metadata += ' - #feedolist instagram photos';
                */
                res.render('index', {
                    title: utilities.formatTitle(usersInPosts),
                    users: usersInPosts,
                    metadata: utilities.formatMetadata(usersInPosts, postsList),
                    htmlToTable: utilities.convertJSONToHTMLToFillTable(postsList)
                });
            } else {
                renderEmpty(res);
            }
        }
    );
}


router.get('/top_lists', function(req, res) {
    res.render('top_lists', {
        errors: null
    });
});

router.get('/top_last_lists', function(req, res) {
    res.render('top_last_lists', {
        errors: null
    });
});


module.exports = router;