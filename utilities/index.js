var badUsersList = ['pornhub', 'porn', 'bitsysbikinis'];

function badUserNames(username) {

    var badUserName = false;

    if (username !== undefined) {
        if (Array.isArray(username)) {
            for (var i = 0; i < username.length; ++i) {
                for (var j = 0; j < badUsersList.length; ++j) {
                    if (username[i] === badUsersList[j]) {
                        return true;
                    }
                }
            }
        } else {
            for (var j = 0; j < badUsersList.length; ++j) {
                if (username === badUsersList[j]) {
                    return true;
                }
            }
        }
    }
    return false;
}

function formatTitle(listOfUsers) {
    var title = '';
    listOfUsers.forEach(user => {
        if (user) title += user.name + ' ';
    });
    /// Update top lists
    title += '- instagram lists #feedolist';
    return title;
}

function formatRequest(usernames) {
    var dummy = [];
    if (Array.isArray(usernames)) {
        dummy = usernames;
    } else {
        dummy.push(usernames);
    }
    var urlRequest = "";
    dummy.forEach(user => {
        urlRequest += 'user=' + user + "&";
    });
    return urlRequest;
}

function formatMetadata(users, posts) {
    var metadata = '';
    if (users) {
        users.forEach(user => {
            if (user) {
                metadata += user.name ? user.name : user.username;
                metadata += ' ';
            }
        });
    }
    metadata += ' Instagram posts';

    var text = '';
    if (posts) {
        posts.forEach(post => {
            if (post) {
                if (text.length < post.text.length && post.text.length < 320) {
                    text = post.text;
                }
            }
        });
    }
    metadata += ' | ' + text;
    return metadata;
}

function addUrlToUsernames(text) {
    var dummy = text.split(' ');
    for (j = 0; j < dummy.length; ++j) {
        if (dummy[j].includes('@') === true) {
            var user = '@' + dummy[j].substring(dummy[j].indexOf('@')).replace(/[^a-zA-Z0-9]+/g, '');
            //console.log("User: " + user);
            if (user.length > 1) {
                text = text.replace(user, " <a href='/?user=" + user.replace('@', '') + "'>" + user + "</a>");
            }
        }
    }

    return text;
}

function convertJSONToHTMLToFillTable(fullUsersList) {
    var html = "";
    var index = 0;
    for (var i = 0; i < fullUsersList.length; ++i) {

        index = i + 1;

        if (index === 1 || (index - 1) % 3 === 0) {
            html += '<div class="row" id="usersImages" style="margin-top: 20px;">';
        }

        fullUsersList[i].text = addUrlToUsernames(fullUsersList[i].text);

        html += '<div class="col-sm">';
        html += '<div class="card">';
        html += '<img class="card-img-top figure-img img-fluid rounded" src="' + fullUsersList[i].imageUrl + '">';
        html += '<div class="card-body">';
        html += '<h5 class="card-title">' + fullUsersList[i].username + '</h5>'
        html += '<p class="card-text">' + fullUsersList[i].text + "</p>"
        html += '</div>';
        html += '</div>';
        html += '</div>';

        if (index % 3 === 0) {
            html += '</div>';
        }

    }
    return html;
}

module.exports.badUserNames = badUserNames;
module.exports.formatTitle = formatTitle;
module.exports.formatRequest = formatRequest;
module.exports.formatMetadata = formatMetadata;
module.exports.convertJSONToHTMLToFillTable = convertJSONToHTMLToFillTable;