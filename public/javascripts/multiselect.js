$('#usersSelectionList').select2({
    dropdownAutoWidth: true,
    width: '90%',
    allowClear: true,
    placeholder: "Search up to seven Instagram users (Ex:Cristiano)",
    minimumInputLength: 3,
    escapeMarkup: function(m) {
        return m;
    },
    ajax: {
        async: true,
        crossDomain: true,
        dataType: "jsonp",
        url: serverAddress + 'users_list',
        data: function(params) {
            return {
                username: params.term.toLowerCase()
            };
        },
        processResults: function(data, params) {
            params.page = params.page || 1;

            return {
                results: data.users,
                pagination: {
                    more: (params.page * 10) < data.count_filtered
                }
            };
        }
    },
    templateResult: function(data, term) {
        if (data.loading) return "Searching...";
        return '<div id="roundphoto"><span><img id="roundrect" src="' + data.picture + '" height=50px width=50px /> ' + data.username + '</span></div>';
    },
    templateSelection: function(data, term) {
        if (data.picture) {
            return '<div id="roundphoto"><span class="selectedUser"><img id="roundrect" src="' + data.picture + '" height=50px width=50px /> ' + data.username + '</span></div>';
        } else {
            var user = JSON.parse(data.text);
            return '<div id="roundphoto"><span class="selectedUser"><img id="roundrect" src="' + user.picture + '" height=50px width=50px /> ' + user.username + '</span></div>';
        }
    }
});