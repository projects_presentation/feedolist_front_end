function fillTopListsTable() {
    var html = "";
    $.ajax({
        type: "GET",
        url: serverAddress + "web/lists/top_lists/",
        async: false,
        success: function(result) {
            for (var i = 0; i < result.top_lists.length; ++i) {
                html += "<tr><td>" + (i + 1) + "</td><td>" + result.top_lists[i].count + "</td><td><a href='" + result.top_lists[i].url + "'>" + result.top_lists[i].users + "</a></td></tr>";
            }
        },
        error: function(result) {}
    });
    return html;
}

function fillTopLastListsTable() {
    var html = "";
    $.ajax({
        type: "GET",
        url: serverAddress + "web/lists/top_last_lists/",
        async: false,
        success: function(result) {
            for (var i = 0; i < result.top_last_lists.length; ++i) {
                html += "<tr><td>" + (i + 1) + "</td><td>" + result.top_last_lists[i].count + "</td><td><a href='" + result.top_last_lists[i].url + "'>" + result.top_last_lists[i].users + "</a></td></tr>";
            }
        },
        error: function(result) {}
    });
    return html;
}