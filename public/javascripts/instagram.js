function getUsersPosts(buttonEvent) {

    buttonEvent.preventDefault();
    var usersArray = $("#usersSelectionList").select2('data');
    var urlToAdd = '?';
    for (var i = 0; i < usersArray.length; ++i) {
        urlToAdd += 'user=';
        if (usersArray[i].username) {
            urlToAdd += usersArray[i].username + (i + 1 === usersArray.length ? '' : '&');
        } else {
            urlToAdd += JSON.parse(usersArray[i].text).username + (i + 1 === usersArray.length ? '' : '&');
        }
    }

    var landingUrl = "https://" + window.location.host + "/" + urlToAdd;
    window.location.href = landingUrl;

}

function fillMultiSelect(users) {

    users.forEach(user => {
        var newOption = new Option(JSON.stringify(user), user.id, false, true);
        $('#usersSelectionList').append(newOption).trigger('change');
    });
}