function fillUsersTable(data) {
    $('#usersImages').html('');
    var userdata = jQuery.parseJSON(data);
    var html = "";
    var resultsLabelText = "";
    var rowCounter = 0;
    var counter = 0;
    for (var i = 0; i < userdata.length; ++i) {
        if (!resultsLabelText.includes(userdata[i].username)) {
            resultsLabelText += userdata[i].username + " ";
        }
        if (rowCounter === 0)
            html += '<div class="row" id="usersImages">';

        html += '<div class="col-md-4 animated pulse bd-centered">';
        html += '<div class="thumbnail">';
        html += '<a href="' + userdata[i].link + '" target="_blank">';
        html += '<img src="' + userdata[i].imageURL + '" class="img-responsive rounded thumbnail" height="300" width="300"/></a>';
        html += '<div class="caption well">' + userdata[i].username + '<br>Likes: ' + userdata[i].likesCounter + '<br>' + userdata[i].text + '</div>';
        html += '</div></div>';
        if (rowCounter++ === 2) {
            rowCounter = 0;
            html += '</div>';
        }
        //if(++counter >= 12) break;
    }
    var usersImages = document.getElementById('usersImages');
    usersImages.innerHTML = html;
    //$('#usersImages').html(html);
}
